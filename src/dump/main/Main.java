package dump.main;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.List;

import dump.Parser;
import dump.Scanner;
import dump.ast.Conflict;
import dump.ast.Graph;
import dump.ast.State;
import dump.ast.Transition;

public class Main {
	public static void main(String[] args) {
		try {
			// FIXME Debug
			//args = new String[] { "Examples/Else.cup.dump" };
			//args = new String[] { "--help" };
			
			CmdOptions opts = parseCmdLine(args);
			
			Scanner s = new Scanner(new FileReader(opts.input));
			Parser p = new Parser();
			Graph g = (Graph) p.parse(s);
			
			if (opts.info) {
				float lrItems = 0;
				float transitions = 0;
				int maxDistance = 0;
				ArrayList<Transition> longestString, temp;
				longestString = null;
				for( State state: g.getStateList() )  {
					lrItems += state.getNumItem();
					transitions += state.getNumTransition();
				}
				for( int i = 0; i < g.getNumState(); i++ ) {
					temp = g.BFSFind(0,i);
					if( temp.size() > maxDistance ) {
						longestString = temp;
						maxDistance = temp.size();
					}
				}
				System.out.printf("The graph has %d states with an average of %.1f lr_items\n", g.getNumState(), lrItems / g.getNumState());
				System.out.printf("and an average of %.1f transitions and a total of %.0f transitions.\n", transitions / g.getNumState(), transitions);
				System.out.printf("The longest shortest path from the start state to any other state is %d\n", maxDistance-1);
				System.out.printf("A string representing the path from the start state to this state is:\n");
				for( Transition t: longestString ) {
					System.out.printf("%s ", t.getIdUse());
				}
				System.out.println();
			}

			if (opts.dotgraph) {
				g.saveAsDot(opts.getOutputFile(".dot"));
			}
			if (opts.conflict) {
				Set<Conflict> conflicts = g.conflicts();
				List<Transition> pathToConflict = null;
				
				if(conflicts.isEmpty()) {
					System.out.println("No Conflicts found!");
				} else {
					System.out.println("Found " +conflicts.size() + " conflict"+(conflicts.size()==1?"":"s")+":\n");
					for(Conflict c: conflicts) {
						System.out.println(c.prettyString()); 
					}

					//State start = (State)g.getStates().getChild(0);
					//start.getTransitions().add(new Transition(new IdUse("383"), new Num("383")));
//					pathToConflict = g.BFSFindConflict(0);
					
					pathToConflict = g.findConflictPath(conflicts.iterator().next());
					if( pathToConflict != null ) {
						System.out.print("State " + pathToConflict.get(pathToConflict.size()-1).getNum());
						System.out.println(" has a conflict. A prefix is: ");
						for( Transition t : pathToConflict )
							System.out.print(t.getIdUse() + " ");
						System.out.println();
						
					}
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Parser.Exception e) {
			e.printStackTrace();
		} catch (InvalidCmdOptionsException e) {
			printHelpMessage(e.getMessage());
		}
	}
	
	private static void printHelpMessage(String message) {
		String usage = "LR conflict demonstrator\n"
				+ (message.isEmpty() ? "" : "Error: " + message + "\n")
				+ "Usage:\n\n"
				+ "\tjava -jar <jarfile> [OPTIONS...] [infile]\n"
				+ "\tjava -jar <jarfile> --help\n"
				+ "\n"
				+ (message.isEmpty() ? "\t-i [file] or --input [file]\n"
						+ "\t\tSpecify input file.\n\n"
						+ "\t-o [file] or --output [file]\n"
						+ "\t\tSpecify output file. (Optional)\n\n"
						+ "\t--info\n"
						+ "\t\tprints some basic info about the graph\n\n"
						+ "\t--dotgraph\n"
						+ "\t\tDump the graph in dot format (Using the output filename).\n\n"
						+ "\t--conflict\n"
						+ "\t\tPrint the number of the first conflict state found. (Default)\n\n"
						+ "\t-h or --help\n"
						+ "\t\tShow this help message."
						: "");
		System.err.println(usage);
	}
	
	private static class CmdOptions {
		public String input;
		public String output;
		public boolean conflict;
		public boolean dotgraph;
		public boolean info;
		
		// If specified returns output filename otherwise generates from input
		// filename
		public String getOutputFile(String ext) {
			if (output != null) {
				return output;
			}
			int boundry = Math.max(input.lastIndexOf("/"),
					input.lastIndexOf("\\"));
			int pos = input.lastIndexOf(".");
			if (pos == -1 || pos < boundry) {
				pos = input.length();
			}
			String out = input.substring(0, pos) + ext;
			return out;
		}
	}
	
	public static CmdOptions parseCmdLine(String[] args)
			throws InvalidCmdOptionsException {
		CmdOptions opts = new CmdOptions();
		int n = 0;
		while (n < args.length) {
			switch (args[n]) {
			case "--donothing":
				System.exit(0);
			case "-o":
			case "--output":
				if (opts.output != null) {
					throw new InvalidCmdOptionsException(
						"Output filename specified twice."
					);
				}
				// Next argument is assumed to be a filename for output file
				n++;
				if (n >= args.length) {
					// Missing filename argument
					throw new InvalidCmdOptionsException(
						"Missing output filename."
					);
				}
				
				opts.output = args[n];
				
				break;
			case "-i":
			case "--input":
				if (opts.input != null) {
					throw new InvalidCmdOptionsException(
						"Input filename specified twice."
					);
				}
				// Next argument is assumed to be a filename for input file
				n++;
				if (n >= args.length) {
					// Missing filename argument
					throw new InvalidCmdOptionsException(
						"Missing input filename."
					);
				}
				
				opts.input = args[n];
				
				break;
			case "--info":
				opts.info = true;
				break;
			case "--dotgraph":
				opts.dotgraph = true;
				break;
			case "--conflict":
				opts.conflict = true;
				break;
			case "-h":
			case "--help":
				throw new InvalidCmdOptionsException("");
			default:
				if (args[n].charAt(0) == '-') {
					throw new InvalidCmdOptionsException(
						"Unknown option: " + args[n]
					);
				} else if (opts.input != null) {
					throw new InvalidCmdOptionsException(
						"Input filename specified twice."
					);
				}
				// Assuming this is the input filename
				
				opts.input = args[n];
				
				break;
			}
			n++;
		}
		// Validate Options:
		
		// Input file must be specified
		if (opts.input == null) {
			throw new InvalidCmdOptionsException(
					"Input filename not specified.");
		}
		// Input file must exist
		if (!new File(opts.input).exists()) {
			throw new InvalidCmdOptionsException(
					"Input file does not exist (or cannot be read).");
		}
		// Must do something
		if (!opts.conflict && !opts.dotgraph) {
			// Default to conflict
			opts.conflict = true;
		}
		
		return opts;
	}
	
	private static class InvalidCmdOptionsException extends java.lang.Exception {
		public InvalidCmdOptionsException(String message) {
			super(message);
		}
	}
	
}
