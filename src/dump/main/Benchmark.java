package dump.main;

import dump.main.Main;
import java.lang.management.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;


class Benchmark {

	public static void main(String[] args) {
		PrintStream out = System.out;
		System.setOut(new NullPrintStream());
		long cpuStart, cpuFin;
		int k = 10;
		int throwaway = 10;
		double threshold = 0.01;
		DescriptiveStatistics ds = new DescriptiveStatistics();
		ds.setWindowSize(k);
		int i = 1;
		do{
			cpuStart = getCpuTime();
			Main.main(args);
			cpuFin = getCpuTime();
//			System.gc();
			try {
//				Thread.sleep(150);
			} catch(Exception e) {}
			ds.addValue(cpuFin - cpuStart);
			out.println("Run " + i + " completed");
			i++;
		} while( i < k || ds.getStandardDeviation() / ds.getMean() > threshold );
		out.println("Steady state reached");
		out.printf("The mean of the last %d runs is %f s\n", k, ds.getMean()/1e9);
		System.setOut(out);
	}

	public static long getCpuTime() {
	    ThreadMXBean bean = ManagementFactory.getThreadMXBean( );
	    return bean.isCurrentThreadCpuTimeSupported( ) ?
		bean.getCurrentThreadCpuTime( ) : 0L;
	}

	static class NullPrintStream extends PrintStream {

		public NullPrintStream() {
			super(new NullByteArrayOutputStream());
		}

		private static class NullByteArrayOutputStream extends ByteArrayOutputStream {

			@Override
			public void write(int b) {
				// do nothing
			}

			@Override
			public void write(byte[] b, int off, int len) {
				// do nothing
			}

			@Override
			public void writeTo(OutputStream out) throws IOException {
				// do nothing
			}

		}

	}
}
