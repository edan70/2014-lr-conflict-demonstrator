package dump;

import beaver.*;
//import beaver.Symbol;
//import beaver.Scanner;
import dump.Parser;
import dump.Parser.*;

%%

// define the signature for the generated scanner
%public
%final
%class Scanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol 
%function nextToken 

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
%}

SRWarning = "Warning : *** Shift/Reduce conflict found in state #"
Start = "START"
WhiteSpace = [ ] | \t | \f | \r | \n | \r\n
Transition = "transition on"
ToState = "to state"
Number = [0-9]+
Identifier = [a-zA-Z$_][a-zA-Z0-9$_]*
StateHeader = "lalr_state"
Divider = ---+
PosMarker = "(*)"
End = "------- CUP"

// Summary = "------- CUP .* Parser Generation Summary -------.*"
// ConflictDetails = "between [^]* Resolved in favou?r of shifting."

%state START

%%

<YYINITIAL> {
	{Start}			{ yybegin(START); return sym(Terminals.START); }
	[^]	{}

	<<EOF>>			{ return sym(Terminals.EOF); }
	
}

<START> {
  {WhiteSpace} {}
  {End} { yybegin(YYINITIAL); }

	"["				{ return sym(Terminals.LBRACKET); }
	"]"				{ return sym(Terminals.RBRACKET); }
	"{"				{ return sym(Terminals.LCURLY); }
	"}"				{ return sym(Terminals.RCURLY); }
	","				{ return sym(Terminals.COMMA); }
	"::="			{ return sym(Terminals.DEFINE); }
	":"				{ }
	{StateHeader}	{ return sym(Terminals.STATE_HEADER); }
	{Number}		{ return sym(Terminals.NUMBER); }
	{Identifier}	{ return sym(Terminals.IDENTIFIER); }
	{PosMarker}		{ return sym(Terminals.POS_MARKER); }
	{Divider}		{ return sym(Terminals.DIVIDER); }
	{SRWarning}		{ }
	{Transition}	{ return sym(Terminals.TRANSITION_ON); }
	{ToState}		{ return sym(Terminals.TO_STATE); }
	
	[^]				{ throw new SyntaxError("Illegal character <"+yytext()+">"); }
}
