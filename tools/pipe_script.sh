#!/bin/zsh
if [ -n $2 ]; then
	RUNS=30
else
	RUNS=$2
fi 
echo $2
echo $RUNS
for x in {1..$RUNS}; do
	java -cp bin:lib/beaver-rt.jar:lib/commons-math3-3.3.jar dump.main.Benchmark $1 | grep mean | perl -ne 'print "$1\n" if /([0-9]+\.[0-9]+)/'
	echo "run $x completed" >&2

done | python tools/confidence.py

