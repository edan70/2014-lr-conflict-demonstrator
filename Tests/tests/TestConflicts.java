package tests;

import static org.junit.Assert.assertFalse;

import java.io.FileReader;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import dump.Parser;
import dump.Scanner;
import dump.ast.Graph;
import dump.ast.Transition;

public class TestConflicts {

	Graph g;
	
	@Before
	public void setUp() throws Exception {
		String file = "Examples/Else.cup.dump";
		Scanner s = new Scanner(new FileReader(file));
		Parser p = new Parser();
		g = (Graph) p.parse(s);
	}

	@Test
	public void testHasConflict() {
		assert(g.getState(5).hasConflicts());
		
	}
	
	@Test
	public void testHasNoConflicts() {
		assertFalse(g.getState(1).hasConflicts());
		
	}
	
	@Test
	public void testFindConflict() {
		ArrayList<Transition> path = g.BFSFindConflict(g.getState(0));
		assert(path.get(path.size()-1).getState().hasConflicts());
	}

}
