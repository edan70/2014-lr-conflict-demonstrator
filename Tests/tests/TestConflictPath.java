
package tests;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import dump.Parser;
import dump.Scanner;
import dump.ast.*;

public class TestConflictPath {

	Graph g;
	
	@Before
	public void setUp() throws Exception {
		String file = "Examples/Else.cup.dump";
		Scanner s = new Scanner(new FileReader(file));
		Parser p = new Parser();
		g = (Graph) p.parse(s);
	}

	@Test
	public void testConflictPath() {
		Set<Conflict> conflicts = g.conflicts();
		Conflict c = conflicts.iterator().next();
		List<Transition> transitions = g.findConflictPath(c);
		String prefix = "";
		for( Transition t: transitions ) {
			prefix += t.getIdUse() + " ";
		}
		//System.out.println(prefix);
		assertEquals("IF Expr IF Expr Stmt ELSE ", prefix);
	}

}

