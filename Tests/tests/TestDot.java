package tests;

import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

import dump.Parser;
import dump.Scanner;
import dump.ast.Graph;

public class TestDot {


	Graph g;
	
	@Before
	public void setUp() throws Exception {
		String file = "Examples/Else.cup.dump";
		Scanner s = new Scanner(new FileReader(file));
		Parser p = new Parser();
		g = (Graph) p.parse(s);
	}
	
	@Test
	public void testDot() throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		g.generateDot(ps);
		String content = baos.toString("utf-8");
		assert(content.startsWith("digraph G"));
	}

}
