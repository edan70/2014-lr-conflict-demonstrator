package tests;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import dump.Parser;
import dump.Scanner;
import dump.ast.Graph;
import dump.ast.State;
import dump.ast.Transition;

public class TestBFS {

	Graph g;
	
	@Before
	public void setUp() throws Exception {
		String file = "Examples/Else.cup.dump";
		Scanner s = new Scanner(new FileReader(file));
		Parser p = new Parser();
		g = (Graph) p.parse(s);
	}

	@Test
	public void testBFS() {
		int goal = 5;
		ArrayList<Transition> path = g.BFSFind(g.getState(0),g.getState(goal));
		assertEquals(goal,path.get(path.size()-1).getNum().num());
	}
	
	@Test
	public void TestBFSFindSet() {
		HashSet<State> goals = new HashSet<State>();
		goals.add(g.getState(3));
		goals.add(g.getState(4));
		goals.add(g.getState(5));
		ArrayList<Transition> path = g.BFSFindSet(g.getState(0), goals);
		assert(goals.contains(path.get(path.size()-1)));
	}

}
