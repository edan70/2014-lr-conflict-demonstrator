#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"
echo CUP is creating Dumps of LR-graphs from the grammars

for file in *.cup; do
	java -jar ../tools/java-cup-11b.jar -dump_states $file >$file.dump 2>&1
done

LRJAR=../dist/release.jar

if [ -f $LRJAR ]; then	
	echo Lr Conflict demonstrator is generating prefixes.
else
	echo LR conflict demonstrator was not found.
	echo Try building project first.
	exit 1
fi


for file in *.cup.dump; do
	java -jar $LRJAR -i $file >${file%.*}.out
done

echo Lr Conflict demonstrator is generating dot-graphs.

for file in *.cup.dump; do
	java -jar $LRJAR -i $file --dotgraph -o ${file%.*}.dot
done




